//
//  ViewController.m
//  Recipe App using Local Storage
//
//  Created by Alex Brown on 5/31/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
        //PList
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Property List" ofType:@"plist"];
    arrayOfRecipeInfo = [[NSArray alloc]initWithContentsOfFile:filePath];
    
        //UITable view
    
    UITableView* tableView = [[UITableView alloc]initWithFrame:self.view.bounds style: UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];

}

        //Add rows to table depending on dict.count

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrayOfRecipeInfo.count;
}

        //Populate tableview with corresponding data from PList

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSDictionary* infoDictionary = arrayOfRecipeInfo[indexPath.row];
    
    cell.textLabel.text = [infoDictionary objectForKey:@"Recipe"];
    
    return cell;
}

        //

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary* infoDictionary = arrayOfRecipeInfo[indexPath.row];
    

    infoViewController* info = [infoViewController new];
    info.infoDictionary = infoDictionary;
    [self.navigationController pushViewController:info animated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
