//
//  ViewController.h
//  Recipe App using Local Storage
//
//  Created by Alex Brown on 5/31/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoViewController.h"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate> {
    //Globally declared array of PList items
    NSArray* arrayOfRecipeInfo;
}



@end

