//
//  infoViewController.m
//  Recipe App using Local Storage
//
//  Created by Alex Brown on 5/31/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "infoViewController.h"

@interface infoViewController ()

@end

@implementation infoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        
    //Information display
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Recipe title lable
    
    UILabel* titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width,30)];
    titleLbl.text = [self.infoDictionary objectForKey:@"Recipe"];
    titleLbl.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:titleLbl];
    
    //Recipe image
    
    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 130, self.view.frame.size.width, 300)];
    iv.contentMode = UIViewContentModeScaleAspectFit;
    [iv setImage: [UIImage imageNamed: [self.infoDictionary objectForKey:@"Image"]]];
    [self.view addSubview:iv];
    
    //Recipe directions label
    
    UILabel* directionsTxt = [[UILabel alloc]initWithFrame:CGRectMake(0, 180, self.view.frame.size.width,self.view.frame.size.height)];
    directionsTxt.text = [self.infoDictionary objectForKey:@"Description"];
    directionsTxt.textAlignment = NSTextAlignmentCenter;
    directionsTxt.lineBreakMode = NSLineBreakByWordWrapping;
    directionsTxt.numberOfLines = 0;
    [self.view addSubview:directionsTxt];}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
