//
//  infoViewController.h
//  Recipe App using Local Storage
//
//  Created by Alex Brown on 5/31/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface infoViewController : UIViewController

@property (nonatomic, retain)
NSDictionary* infoDictionary;


@end
